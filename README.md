## _Engage_ module for node-red

Allows communicating with the Engage API from node-red.

This assumes you have [Node-RED](https://nodered.org) already installed and working, if you need to install Node-RED see [here](https://nodered.org/docs/getting-started/installation)

**NOTE:** This requires [Node.js](https://nodejs.org) v8.11.1+ and [Node-RED](https://nodered.org/) v0.19+.


### Installation

Install via Node-RED Manage Palette

```
node-red-contrib-engage
```

Install via npm

```shell
$ cd ~/.node-red
$ npm node-red-contrib-engage
# Restart node-red
```

### Usage 
* Drag the `engage` node from the `Engage` category of the node pallet, into your flow.
* Double tap the node instance and configure your credentials via the server configuration config-node:
    ![image](config-node.png)
* Choose the api & operation you would like to execute & fill in required parameters for the chosen API operation.
* **Note** in case the API request ended with an error, it will be passed on to a Node-RED `catch` node.
       
![image](example-form.png)

### Documentation
##### You may find documentation for each operation in the API by viewing the node info section in Node-RED
![image](documentation.png)
