const path = require('path')
    , SwaggerClient = require('swagger-client')
    , yaml = require('yaml-js')
    , fsPromise = require('fs-promise')
    , CssEscape = require('css.escape')
    , _ = require('lodash')
    ,{DateTime}=require('luxon')
;

const nodeName = "engage";
const swaggerSpecFile = "engage.json";

const process = require('process');

const staticConfigParams = new Set(
    ['app-type']
);

function SwaggerSpecManager() {
    this._swaggerSpec = null;
    this._info = null;
}

SwaggerSpecManager.prototype.getInfo = async function () {
    if (this._info !== null) return this._info;

    const choices = {};
    const allParams = {};
    const swaggerClient = await (new SwaggerClient({spec: await this.getSwaggerSpec()}));
    for (const url of Object.keys(swaggerClient.spec.paths)) {
        const methods = swaggerClient.spec.paths[url];
        for (const httpMethodName of Object.keys(methods)) {
            const operationDef = methods[httpMethodName];
            if (!operationDef.operationId || operationDef === 'swagger_raw' || httpMethodName === 'x-swagger-router-controller') continue; // we only care about http methods such as GET POST PUT DELETE...
            const operationId = operationDef.operationId;
            const parameters = {};
            if (operationDef.parameters) {
                for (const paramInfo of operationDef.parameters) {
                    parameters[paramInfo.name] = {
                        required: paramInfo.required,
                        in: paramInfo.in,
                        description: paramInfo.description,
                        schema: paramInfo.schema
                    };
                    if (!allParams[paramInfo.name]) allParams[paramInfo.name] = parameters[paramInfo.name];
                }
            }

            const apiName = (operationDef.tags && operationDef.tags.length) ?
                operationDef.tags[0] :
                'default';
            if (!choices[apiName]) choices[apiName] = {};

            const operationDescription = {url, httpMethodName, parameters, description: operationDef.description};
            if (operationDef.requestBody) {
                operationDescription.requestBody = operationDef.requestBody;
            }
            choices[apiName][operationId] = operationDescription;
        }
    }

    this._info = {choices: choices, allParams: allParams};
    return this._info;
};

SwaggerSpecManager.prototype.getSwaggerSpec = async function () {
    if (this._swaggerSpec !== null) return this._swaggerSpec;

    const swaggerSpecPath = path.resolve(__dirname, 'static', swaggerSpecFile);
    this._swaggerSpec =
        swaggerSpecFile.toLowerCase().endsWith('.json') ?
            require(swaggerSpecPath) :
            yaml.load(await fsPromise.readFile(swaggerSpecPath, 'UTF-8'));

    return this._swaggerSpec;
};

const swaggerSpecManager = new SwaggerSpecManager();

module.exports = async function (RED) {

    const debuglength = RED.settings.debugMaxLength || 1000;
    function sendDebug(msg) {
        msg = RED.util.encodeObject(msg,{maxLength:debuglength});
        RED.comms.publish("debug",msg);
    }

    function evaluateNodePropertyPromise(value, type, node, msg) {
        return new Promise(function (resolve, reject) {
            RED.util.evaluateNodeProperty(value, type, node, msg, function (err, val) {
                err ? reject(err) : resolve(val);
            })
        })
    }

    function SwaggerApiNode(config) {
        RED.nodes.createNode(this, config);
        (async () => {
            const node = this;

            // Retrieve the config node
            this.server = RED.nodes.getNode(config.server);

            let requestsCounter = 0;

            function updatePendingRequests(inc) {
                requestsCounter += inc ? 1 : -1;
                if (requestsCounter <= 0) node.status({});
                else {
                    node.status({fill: "blue", shape: "dot", text: "Pending Requests: " + requestsCounter});
                }
            }

            function getLoggingCfg() {
                let cfg = {};
                if(node.server?.logging) {
                    Object.assign(cfg, JSON.parse(JSON.stringify(node.server.logging)));
                }
                try {
                    const envVarName = node.server.loggingCfgFromEnvVar.trim();
                    const envVarLoginCfg = JSON.parse(process.env[envVarName]);
                    if(envVarLoginCfg.request) {
                        if(!cfg.request) cfg.request = {};
                        Object.assign(cfg.request, envVarLoginCfg.request)
                    }
                    if(envVarLoginCfg.response) {
                        if(!cfg.response) cfg.response = {};
                        Object.assign(cfg.response, envVarLoginCfg.response)
                    }
                } catch(e) {
                    // Env var does not contain a valid json, so using only from config node value
                }
                return cfg;
            }

            let swaggerClient;
            let lastReq;

            function tryParseVerticalFromUrl(url) {
                try {
                    const regex = /verticals\/[a-z]*\//g;
                    const found = url.match(regex);
                    if (found.length) {
                        return found[0].replace("verticals/", "").replace("/", "")
                    }
                    return null;
                } catch (e) {
                    return null
                }
            }

            function baseRequestInterceptor(req, msg) {
                lastReq = req;

                let vertical = tryParseVerticalFromUrl(req.url);
                if ((req.headers)) {
                    if (req.headers.segment) {
                        vertical = req.headers.segment
                    } else if (req.headers.vertical) {
                        vertical = req.headers.vertical
                    }
                }

                if (node.server && node.server.token && vertical && node.server.token[vertical]) {
                    req.headers['Authorization'] = 'Bearer ' + node.server.token[vertical];
                }
                if(node.server && node.server.cookie) {
                    req.headers['Cookie'] = node.server.cookie;
                }
    
                const logReq = Object.assign({}, req);
                if(logReq.body && typeof logReq.body === 'string') {
                    try {
                        logReq.body = JSON.parse(logReq.body);
                    } catch(e) {}
                }
    
                const logging = getLoggingCfg();
                let logStr = `monogoto-hub request (${logReq.method}) ${logReq.url} from ${node.id} ${config['node-label']||''}`;
                if(logging?.request) {
                    let logStr = `Request (${logReq.method}) from ${node.id} ${config['node-label']||''}`;
    
                    const logObj = {method: logReq.method};
                    if(logging?.request?.url) logObj.url = logReq.url
                    if(logging?.request?.headers) logObj.headers = logReq.headers;
                    if(logging?.request?.body) logObj.body = logReq.body;
                    if(Object.keys(logObj).length) {
                        logStr += ` ${JSON.stringify(logObj, null, 2)}`;
                    }
    
                    node.log(logStr);
                    sendDebug({id:node.id, z:node.z, _alias: node._alias,  path:node._flow.path, name:node.name, topic:"Request", 
                        msg: msg? { _msgid: msg._msgid, ...logObj} : undefined
                    });
                }else{
           
                    msg.StartTimeContrib=DateTime.now().toISO()                               
                    node.log({event:"APIRequest",
                    app:{"appName":"monogoto-hub-contrib"},
                    "req":{"url":logReq.url},   
                        "msg":logStr,                    
                        "metaInfo":{"sessionId":msg?.reqPayload?.meta?.metaInfo?.sessionId}
                    });
               
                }
                
                return req;
            }
    
            function baseResponseInterceptor(res, msg) {
    
                const clonedRes = _.cloneDeep(res);
                if(clonedRes.body) {
                    delete clonedRes.data;
                }
                delete clonedRes.text;
                delete clonedRes.obj;
    
                const logging = getLoggingCfg();
                let logStr = `monogoto-hub  Response of ${res.url} " " ${node.id} ${config['node-label']||''}`
                
                if(logging?.response) {
                    let logStr = `Response of ${node.id} ${config['node-label']||''}`
    
                    const logObj = {};
                    if(logging?.response?.headers) logObj.headers = clonedRes.headers;
                    if(logging?.response?.body) logObj.body = clonedRes.body;
                    if(Object.keys(logObj).length) {
                        logStr += ` ${JSON.stringify(logObj, null, 2)}`;
                    }
    
                    node.log(logStr);
                    sendDebug({id:node.id, z:node.z, _alias: node._alias,  path:node._flow.path, name:node.name, topic:"Response", 
                        msg: msg? { _msgid: msg._msgid, ...logObj} : undefined
                    });
                }else{
           
                    // node.log({"msg":logStr,
                    //     "metaInfo":{"sessionId":msg?.reqPayload?.meta?.metaInfo?.sessionId}
                    // });
                    const now =DateTime.now()
                    const startTime=DateTime.fromISO(msg.StartTimeContrib)
                    const endTime=DateTime.fromISO(now)
                    const diff=endTime.diff(startTime)
                    const diffInSeconds = diff.toObject('milliseconds');
                    node.log({event:"APIResponse",
                    app:{"appName":"monogoto-hub-contrib"},
                        "msg":logStr,   
                        "req":{"url":res.url},
                        "swagger":{path:msg.operationInfo.url} ,                  
                        "metaInfo":{
                            "sessionId":msg?.reqPayload?.meta?.metaInfo?.sessionId,
                            'res':{contribTime: diffInSeconds?diffInSeconds.milliseconds:0}
                    }
                    });

                }
                delete msg.StartTimeContrib
                delete msg.operationInfo
                return res;
            }

            try {
                swaggerClient = await SwaggerClient({
                    spec: await swaggerSpecManager.getSwaggerSpec(),
                });
                if (this.server && this.server.host) {
                    const server = !this.server["host-type"] ? this.server.host :
                        await evaluateNodePropertyPromise(this.server.host, this.server["host-type"], this.server, {});

                    if (swaggerClient.spec.servers) {
                        swaggerClient.spec.servers = [{url: server}];
                    } else {
                        swaggerClient.spec.host = server;
                    }
                }
            } catch (reason) {
                node.error("failed to load swagger spec file.\r\n" + reason);
            }

            node.on('input', (async (msg) => {
                let request = null;

                function sendAndSaveRequest(swaggerClientOperation, resolvedParams, extraParams) {
                    const argsToSend = Array.from(arguments).slice(1);
                    const promise = swaggerClientOperation.apply(swaggerClientOperation, argsToSend);
                    request = lastReq;
                    return promise;
                }

                updatePendingRequests(true);
                const sendReq = async (alreadyTriedLogin) => {
                    try {
                        const operationInfo = (await swaggerSpecManager.getInfo()).choices[config.api][config.operation];
                        msg.operationInfo=operationInfo
                        const resolvedParams = {};
                        const operationParamNames = new Set(Object.keys(operationInfo.parameters));
                        for (const paramName of operationParamNames) {
                           
                         
                            const paramValueInField = config['param-' +  CSS.escape(paramName)];
                            const paramTypedInput = config['param-' +  CSS.escape(paramName) + '-type'];

                            resolvedParams[paramName] = await evaluateNodePropertyPromise(paramValueInField, paramTypedInput, node, msg);
                        }
                        
                        const paramsToSetFromConfig = new Set(Array.from(staticConfigParams).filter(x => operationParamNames.has(x)));
                        for (const paramName of paramsToSetFromConfig) {
                            resolvedParams[paramName] = this.server[paramName];
                        }


                        const extraParams = {};
                        if (operationInfo.requestBody) {
                            extraParams.requestBody = await evaluateNodePropertyPromise(config.requestBody, config['requestBody-type'], node, msg);
                        }

                        const swaggerClientOperation = swaggerClient.apis[config.api][config.operation];

                        extraParams.requestInterceptor = function(req) {
                            req = baseRequestInterceptor(req, msg);
                            if(msg?.headers && Object.keys(msg.headers).length) {
                                if(!req.headers || !Object.keys(req.headers).length) req.headers = {};
                                Object.assign(req.headers, msg.headers);
                            }
                            return req;
                        }
    
                        extraParams.responseInterceptor = function(req) {
                            req = baseResponseInterceptor(req, msg);
                            return req;
                        }

                        if (msg.token) {
                            extraParams.requestInterceptor=(req)=>{
                                req.headers.Authorization = 'Bearer ' + msg.token
                            }
                        }
                        const swaggerResponse = await sendAndSaveRequest(swaggerClientOperation, resolvedParams, extraParams);
                        if (swaggerResponse.hasOwnProperty('body') &&
                            (
                                swaggerResponse.body.constructor === {}.constructor ||
                                swaggerResponse.body.constructor === [].constructor
                            )
                        ) {
                            msg.payload = swaggerResponse.body;
                        } else {
                            msg.payload = swaggerResponse.data;
                        }

                        updatePendingRequests(false);
                        node.send(msg);
                    } catch (error) {
                        try {
                            if (error.statusCode === 401 && !alreadyTriedLogin && this.server) {
                                const mobile = await evaluateNodePropertyPromise(this.server.mobile, this.server['mobile-type'], node, msg);
                                const pass = await evaluateNodePropertyPromise(this.server.password, this.server['password-type'], node, msg)

                                const vertical = await (async () => {
                                    try {

                                        const operationInfo = (await swaggerSpecManager.getInfo()).choices[config.api][config.operation];
                                        msg.operationInfo=operationInfo
                                        const operationParamNames = new Set(Object.keys(operationInfo.parameters));

                                        // Some operations use "segment" and some use "vertical", see which one is the current one
                                        let v = "";
                                        let vt = "";
                                        if (operationParamNames.has("segment")) {
                                            v = config['param-segment'];
                                            vt = config['param-segment-type']
                                        } else if (operationParamNames.has("vertical")) {
                                            v = config['param-vertical']
                                            vt = config['param-vertical-type']
                                        } else {
                                            throw new Error("Must have segment/vertical!");

                                        }

                                        return await evaluateNodePropertyPromise(v, vt, node, msg);
                                    } catch (e) {
                                    }
                                    return undefined;
                                })();

                                const clientResponse = await sendAndSaveRequest(swaggerClient.apis.client.createClient, {
                                    "app-type": this.server['app-type'],
                                    segment: vertical
                                }, {
                                    requestBody: {
                                        deviceId: (RED.util.generateId() + RED.util.generateId())
                                    }
                                });
                                if (this.server) {
                                    this.server.clientId = clientResponse.body.clientId;
                                    this.server.clientSecret = clientResponse.body.clientSecret;
                                }

                                const oauthTokenResponse = await sendAndSaveRequest(swaggerClient.apis.auth.tokenV2, {
                                    "app-type": this.server['app-type'],
                                    segment: vertical
                                }, {
                                    requestBody: {
                                        client_id: this.server.clientId,
                                        client_secret: this.server.clientSecret,
                                        grant_type: "password",
                                        password: pass,
                                        scope: vertical,
                                        username: mobile
                                    }
                                });
                                if (this.server) {
                                    if (!this.server.token) {
                                        this.server.token = {};
                                    }
                                    if (!this.server.refresh_token) {
                                        this.server.refresh_token = {};
                                    }
                                    this.server.token[vertical] = oauthTokenResponse.body.access_token;
                                    this.server.refresh_token[vertical] = oauthTokenResponse.body.refresh_token;
                                }
                                setImmediate(sendReq.bind(this), true);
                            } else {
                                throw error;
                            }
                        } catch (error) {
                            msg.payload = {};
                            if (error.response) {
                                msg.payload.response = _.cloneDeep(error.response);

                                // Cleanup
                                delete msg.payload.statusCode;
                                if (msg.payload.hasOwnProperty('response')) {
                                    delete msg.payload.response.statusCode;
                                    delete msg.payload.response.statusText;
                                    delete msg.payload.response.text;
                                    delete msg.payload.response.obj;
                                    if (msg.payload.response.body) {
                                        delete msg.payload.response.data;
                                    }
                                }

                                if (request) {
                                    delete request.requestInterceptor;
                                    delete request.responseInterceptor;
                                    if (request.body && typeof request.body === 'string') {
                                        try {
                                            request.body = JSON.parse(request.body);
                                        } catch (e) {
                                        }
                                    }
                                    msg.payload.request = request;
                                }
                            } else {
                                msg.payload.error = error;
                            }

                            updatePendingRequests(false);
                            if(!config.outErrors) {
                                node.error(nodeName, msg);
                            } else {
                                node.send([undefined,msg]);
                            }
                        }
                    }
                };
                setImmediate(sendReq.bind(this), false);
            }));
        })();
    }

    RED.httpAdmin.get('/' + nodeName + '/ui-choices', async function (req, res) {
        res.send(await swaggerSpecManager.getInfo());
    });

    RED.nodes.registerType(nodeName, SwaggerApiNode);

    function SwaggerApiNodeServer(n) {
        RED.nodes.createNode(this, n);

        this.host = n.host;
        this['host-type'] = n['host-type'];

        this.password = n.password;
        this['password-type'] = n['password-type'];

        this.mobile = n.mobile;
        this['mobile-type'] = n['mobile-type'];

        this['app-type'] = n['app-type'];

        this.logging = n.logging;
        this.loggingCfgFromEnvVar = n.loggingCfgFromEnvVar;
    }

    RED.nodes.registerType(nodeName + "-server", SwaggerApiNodeServer);
};
